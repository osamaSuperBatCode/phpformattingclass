<?php

include './classes/class.Format.php';

$format = new format();

echo "<table cellpadding='5'>";
echo "<tr>";
echo "<td bgcolor='#CCCCCC'>TYPE</td>";
echo "<td bgcolor='#DFEFFF'>INPUT</td>";
echo "<td bgcolor='#CAFFCA'>OUTPUT</td>";
echo "</tr>";

echo "<tr><td colspan='3'><hr /></td></tr>";

echo "<tr><td bgcolor='#CCCCCC'>PROVINCE</td>   	<td bgcolor='#DFEFFF'>british columbia</td>   	<td bgcolor='#CAFFCA'>". $format->province("british columbia") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>PROVINCE</td>   	<td bgcolor='#DFEFFF'>bc</td>   			  	<td bgcolor='#CAFFCA'>". $format->province("bc") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>UPPER</td>  		<td bgcolor='#DFEFFF'>the big black cat</td>   	<td bgcolor='#CAFFCA'>". $format->province("the big black cat") ."</td></tr>";

echo "<tr><td colspan='3'><hr /></td></tr>";

echo "<tr><td bgcolor='#CCCCCC'>MONEY</td>   		<td bgcolor='#DFEFFF'>123666</td>   			<td bgcolor='#CAFFCA'>". $format->number("123666") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>MONEY</td>   		<td bgcolor='#DFEFFF'>25.00</td>   				<td bgcolor='#CAFFCA'>". $format->number("25.00") ."</td></tr>";

echo "<tr><td colspan='3'><hr /></td></tr>";

echo "<tr><td bgcolor='#CCCCCC'>FILE SIZE</td>   	<td bgcolor='#DFEFFF'>1024</td>   				<td bgcolor='#CAFFCA'>". $format->fileSize("1024") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>FILE SIZE</td>   	<td bgcolor='#DFEFFF'>10024</td>   				<td bgcolor='#CAFFCA'>". $format->fileSize("10024") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>FILE SIZE</td>   	<td bgcolor='#DFEFFF'>100024</td>   			<td bgcolor='#CAFFCA'>". $format->fileSize("100024") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>FILE SIZE</td>   	<td bgcolor='#DFEFFF'>10000024</td>   			<td bgcolor='#CAFFCA'>". $format->fileSize("10000024") ."</td></tr>";

echo "<tr><td colspan='3'><hr /></td></tr>";

echo "<tr><td bgcolor='#CCCCCC'>DATE</td>   	<td bgcolor='#DFEFFF'>".date("Y-m-d H:i:s")."</td>   <td bgcolor='#CAFFCA'>". $format->humanDate(date("Y-m-d H:i:s")) ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>DATE</td>   	<td bgcolor='#DFEFFF'>".date("Y-m-d H:i:s")."</td>   <td bgcolor='#CAFFCA'>". $format->humanDate(date("Y-m-d H:i:s"), FALSE) ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>DATE</td>   	<td bgcolor='#DFEFFF'>".date("Y-m-d H:i:s")."</td>   <td bgcolor='#CAFFCA'>". $format->humanDate(date("Y-m-d H:i:s"), FALSE, FALSE) ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>DATE</td>   	<td bgcolor='#DFEFFF'>".date("Y-m-d H:i:s")."</td>   <td bgcolor='#CAFFCA'>". $format->humanDate(date("Y-m-d H:i:s"), TRUE, FALSE) ."</td></tr>";

echo "<tr><td colspan='3'><hr /></td></tr>";

$longString = "This is a really long string and want to make sure we trim it when needed.";
$shortString = "This is short.";
echo "<tr><td bgcolor='#CCCCCC'>TRUNCATE</td>   <td bgcolor='#DFEFFF'>This is a really long string and want to make sure we trim it when needed.</td>   <td bgcolor='#CAFFCA'>". $format->truncate($longString, 40, " (Read More)") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>TRUNCATE</td>   <td bgcolor='#DFEFFF'>This is short.</td>   		<td bgcolor='#CAFFCA'>". $format->truncate($shortString, 20, " (Read More)") ."</td></tr>";

echo "<tr><td colspan='3'><hr /></td></tr>";

echo "<tr><td bgcolor='#CCCCCC'>PHONE</td>   	<td bgcolor='#DFEFFF'>(250) 111-2222</td>   		<td bgcolor='#CAFFCA'>". $format->phone("(250) 111-2222") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>PHONE</td>   	<td bgcolor='#DFEFFF'>250111-2222</td>   			<td bgcolor='#CAFFCA'>". $format->phone("250 111-2222") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>PHONE</td>   	<td bgcolor='#DFEFFF'>250 111-2222</td>   			<td bgcolor='#CAFFCA'>". $format->phone("250 111-2222") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>PHONE</td>   	<td bgcolor='#DFEFFF'>250.111.2222</td>   			<td bgcolor='#CAFFCA'>". $format->phone("250.111.2222") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>PHONE</td>   	<td bgcolor='#DFEFFF'>+1 250 111-2222</td>   		<td bgcolor='#CAFFCA'>". $format->phone("+1 250 111-2222") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>PHONE</td>   	<td bgcolor='#DFEFFF'>+1 250 111-BABY</td>   		<td bgcolor='#CAFFCA'>". $format->phone("+1 250 111-BABY") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>PHONE</td>   	<td bgcolor='#DFEFFF'>11 111 222 3333 11</td>   	<td bgcolor='#CAFFCA'>". $format->phone("11 111 222 3333 11") ."</td></tr>";

echo "<tr><td colspan='3'><hr /></td></tr>";

echo "<tr><td bgcolor='#CCCCCC'>URL</td>   	<td bgcolor='#DFEFFF'>www.example.com</td>   		<td bgcolor='#CAFFCA'>". $format->url("www.example.com") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>URL</td>   	<td bgcolor='#DFEFFF'>example.com</td>   			<td bgcolor='#CAFFCA'>". $format->url("example.com") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>URL</td>   	<td bgcolor='#DFEFFF'>http://example.com</td>   	<td bgcolor='#CAFFCA'>". $format->url("http://example.com") ."</td></tr>";
echo "<tr><td bgcolor='#CCCCCC'>URL</td>   	<td bgcolor='#DFEFFF'>http://www.example.com</td>   <td bgcolor='#CAFFCA'>". $format->url("http://www.example.com") ."</td></tr>";

echo "<tr><td colspan='3'><hr /></td></tr>";


echo "</table>";



?>

<td bgcolor='#CAFFCA'