<?php

/********************************************************************
    
   TABLE OF CONTENTS
   =================
   1. The Cleaner
   2. Format Province
   3. Uppercase First
   4. Truncate long paragraphs
   5. Format long numbers or money
   6. Format phone numbers
   7. Friendly File Size Formats
   8. Friendly URLS
   9. Format Dates
   
**********************************************************************/

class format
{
	
	/******************************************************************************
	TOC: 1
	
		THIS CLEANS EVERYTHING UP, FOR DB INSERTS
		
		@value = "the string or any input you wish to clean"
	******************************************************************************/
	public function cleanUp($value) 
	{ 
		if(empty($value) || $value == '' || $value == ' ' || $value == NULL)
		{
			return NULL;	
		} else {
			$value = trim($value); //TRIM EXTRA SPACES ON THE ENDS OF THE VARIABLE
			$value = preg_replace(" {2,}", ' ',$value);
			$value = strip_tags($value);  //STRIP TAGS 
			$value = htmlentities($value, ENT_QUOTES);
			$value = mysql_escape_string($value); // ESCAPE STRING, SAFE FOR INSERTING INTO DATABASE
			
			return $value;
			unset ($value);
		}
	}
	
	/******************************************************************************
	TOC: 2
		FORMAT PROVINCE
		
		@value = "the string that you want to make province friendly"
	******************************************************************************/
	public function province($value)
	{
		if($value) 
		{
			// RUN THE CLEANER ON THE VARIABLE BEFORE DOING ANYTHING
			$value = $this->cleanUp($value);
			
			// IF THE USER ONLY INSERTS LESS THAN 4 CHARACTERS
			// IT MOST LIKELY IS THE PROVINCE ACRONYM
			if(strlen($value) < 4)
			{
				// LETS UPPERCASE ALL LETTERS FOR ACRONYMS (ie: BC / USA)
				return strtoupper($this->cleanUp($value));
			} else {
				// IF IT IS MORE THAN 3 CHARACTERS LETS JUST CAPITALIZE
				// THE FIRST LETTER OF EACH WORD
				return ucwords(strtolower($this->cleanUp($value)));	
			}
			unset($value);
		}
	}
	
	/******************************************************************************
	TOC: 3
		SOMETIMES WE JUST LIKE TO HAVE CLEAN DATABASES
		SO LETS UPPERCASE THE FIRST LETTER OF EVERY WORD
		
		@value = "the string that you want to uppsercase"
	******************************************************************************/
	public function upperCaseFirst($value)
	{
		if($value) 
		{
			// RUN THE CLEANER ON THE VARIABLE BEFORE DOING ANYTHING
			$value = $this->cleanUp($value);
			
			return ucwords(strtolower($this->cleanUp($value)));
			unset($value);
		}
	}
	
	/******************************************************************************
	TOC: 4
		WHEN YOU NEED YOU HAVE A LONG STRING, SUCH AS PARAGRAPHS
		AND YOU ONLY WANT TO DISPLAY A PORTION OF THAT PARAGRAPH.
		
		@value = "the paragraph you want to shorten"
		@count = "how many characters you want to display"
		@after = "If you want to show (read more), or whatever you wish"
	*******************************************************************************/
	public function truncate($value, $count, $after = NULL)
	{
		if(strlen($value) > $count) 
		{
			// RUN THE CLEANERS
			$value = $this->cleanUp($value);
			
			$value = substr($value,0,$count);
			$value = substr($value,0,strrpos($value," "));
		
			return $value . $after;	
			unset($value);
		} else {
			return $value;
			unset($value);
		}
	}
		

	/******************************************************************************
	TOC: 5
		FORMAT MONEY or LONG NUMBERS
		
		@value = "the number to convet"
		@decimal = "do you want to show decimal place or not" (default is yes)
	******************************************************************************/
	public function number($value, $decimal = true)
	{
		if($value) 
		{
			$value = $this->cleanUp($value);
			
			if ($decimal) {
				$value = sprintf('%.2f', $value);
			}
			while (true) {
				$replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $value);
				if ($replaced != $value) {
					$value = $replaced;
				} else {
					break;
				}
			}
			return $value;
			unset($value);
		}
	}
	
	/******************************************************************************
	TOC: 6
		FORMAT PHONE NUMBERS
		
		@phone = "the number you want to make friendly"
		@format = Currently we only have 2 formats
				  a. normal phone numbers
				  b. skype numbers
				  		- Skype needs to only have numbers with a 
						  + in the front and the word skype
	******************************************************************************/
	public function phone($phone, $format = NULL, $convert = true, $trim = true)
	{
		if($phone)
		{
			$phone = $this->cleanUp($phone);
			
			if($format != NULL)
			{
				if($format == "skype")
				{
					return "+". preg_replace("/[^0-9A-Za-z]/", "", $phone);
					die();
				}
			} 
			
			// REMOVE ANY CHARACTERS THAT ARE NOT NUMBERS
			$phoneOriginal = $phone;
			$phone = preg_replace("/[^0-9A-Za-z]/", "", $phone);
			
			// CONVERT LETTERS TO NUMBERS
			// EXAMPLE: 1-800-EXAMPLE
			if ($convert == true) {
				$replace = array('2'=>array('a','b','c'),
								 '3'=>array('d','e','f'),
								 '4'=>array('g','h','i'),
								 '5'=>array('j','k','l'),
								 '6'=>array('m','n','o'),
								 '7'=>array('p','q','r','s'),
								 '8'=>array('t','u','v'),
								 '9'=>array('w','x','y','z'));
				
				// REPLACE THE LETTERS WITH NUMBERS
				// Notice this is case insensitive with the str_ireplace instead of str_replace 
				foreach($replace as $digit => $letters) {
					$phone = str_ireplace($letters, $digit, $phone);
				}
			}
	
			// CREATE THE PHONE NUMBER FORMATTING
			if (strlen($phone) == 7) {
				return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1-$2", $phone);
			} elseif (strlen($phone) == 10) {
				return preg_replace("/([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "($1) $2-$3", $phone);
			} elseif (strlen($phone) == 11) {
				return preg_replace("/([0-9a-zA-Z]{1})([0-9a-zA-Z]{3})([0-9a-zA-Z]{3})([0-9a-zA-Z]{4})/", "$1($2) $3-$4", $phone);
			} 
			
			// RETURN THE ORIGINAL PHONE NUMBER
			// ONLY IF THE DIGITS DO NOT EQUAL (7, 10, 11)
			return $phoneOriginal;
			unset($phone,$phoneOriginal);
		} else {
			return '';	
		}
	}
	
	/******************************************************************************
	TOC: 7
		MAKE BYTES EASILY READIBLE
		INPUT: Bytes
		OUTPUT: KB / MB / GB
	******************************************************************************/
	public function fileSize($value) 
    {
		if($value) 
		{
			// RUN THE CLEANER
			$value = $this->cleanUp($value);
			$size = $value / 1024;
			if($size < 1024)
			{
				$size  = number_format($size, 2);
				$size .= ' KB';
			} else {
				if($size / 1024 < 1024) 
				{
					$size  = number_format($size / 1024, 2);
					$size .= ' MB';
				} else if ($size / 1024 / 1024 < 1024) {
					$size  = number_format($size / 1024 / 1024, 2);
					$size .= ' GB';
				} 
			}
			return $size;
			unset($value,$size);
		}
    }
	
	/******************************************************************************
	TOC: 8
		MAKE SURE A URL IS SAFE TO USE
		
		@url = "the string to make sure is 
				user friendly to click"
	******************************************************************************/
	public function url($url)
	{
	  if($url)
	  {
		  if(substr($url, 0, 7) == "http://") {
			  return $url;
		  }
		  elseif(substr($url, 0, 4) == "www.") {
			  return "http://".$url; 	
		  }
		  elseif(substr($url, 0, 4) != "www." && substr($url, 0, 7) != "http://") {
			  return "http://www.".$url;  
		  }
		  else {
			return $url;  
		  }
	  }
	}
	
	
	
	/******************************************************************************
	TOC: 9
		TAKE IN A DATE AND MAKE IT 
		NICELY READIBLE
		
		@dateTime = "the date and time"
		@showTime = "do you want to return the time as well?" (default = yes)
		@human = "makes your date readible" (ie: Wednesday, May the 14th, 2011)
		
	******************************************************************************/
	public function humanDate($dateTime, $showTime = TRUE, $human = TRUE)
	{
		if($dateTime)
		{
			// SET THE TIME
			$dateFormat = strtotime($dateTime);
			
			// CREATE THE FORMAT
			$format1 = "Y-m-d";
			$format2 = "l, F d - Y";
			if($showTime == TRUE) {
				$format1 = $format1 ." H:i:s";
				$format2 = $format2 ." \a\\t g:ia";	
			}
			
			// RETURN THE FORMATTED DATE
			if($human == TRUE) {
				return date($format2, $dateFormat);	
			}
			if($human == FALSE) {
				return date($format1, $dateFormat);	
			}
		}
	}
	
	
}

?>